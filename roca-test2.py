#! /usr/bin/env python3

import sys
from pathlib import Path


# https://gist.github.com/marcan/fc87aa78085c2b6f979aefc73fdc381f
# Credit: https://crypto.stackexchange.com/questions/52292/what-is-fast-prime
generators = [
    (2, 11), (6, 13), (8, 17), (9, 19), (3, 37), (26, 53), (20, 61), (35, 71),
    (24, 73), (13, 79), (6, 97), (51, 103), (53, 107), (54, 109), (42, 127),
    (50, 151), (78, 157),
]

print(len(generators))

# Precalculate residues to speed up check
# -> calculate all possible n mod p where n ^ r mod p = 1
tests = []
for r, p in generators:
    l = []
    for i in range(p):
        if (i ** r) % p == 1:
            l.append(i)
    tests.append((p, set(l)))

print(tests)
print(len(tests))

nominator=denominator=1
o=1

for p, l in tests:
    #denominator*=p
    #nominator*=len(l)
    #print(nominator, denominator)
    o*=len(l)/p
    print(len(l)/p, o)

# tests is equivalent to the original test masks,
# minus red herring entries with all bits set

# Equivalent to the original implementation
def is_vulnerable(modulus):
    return all(modulus % p in l for p, l in tests)

# Slower version, but using the generators directly with no precalc
def is_vulnerable_slower(modulus):
    return all(pow(modulus, r, p) == 1 for r, p in generators)

if len(sys.argv)<2:
    print("Erwarte Dateiname als erstes Argument mit Moduls als Zahl in Hexdezimaldarstellung")
    sys.exit(1)

my_file=Path(sys.argv[1]);

if my_file.exists() and my_file.is_file():
    tmp=my_file.read_text();
    modulus=int(tmp,16)
    print("modulus=0x{0:x}".format(modulus))
else:
    print('Kann Datei "{}" nicht lesen.'.format(sys.argv[1]))
    sys.exit(1)

if is_vulnerable(modulus):
    print("Angreifbar")
else:
    print("OK")

