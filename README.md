# roca

Minimal-Test für ROCA (vgl. https://github.com/crocs-muni/roca), was ich
geschrieben hatte, weil das zunächst veröffentlicht ROCA-Detection-Tool
schwierig in Bezug auf die Dependencies war.  Das Programm roca-test.py
benötigt ausschliesslich eine Standard-Python3-Installation, also ohne
zusätzlich Module installieren zu müssen. Offensichtlich hatten noch mehrere
das Dependency-Problem, den einige Monate später wurde der Test im o. g.
github-Repository nochmal deutlich in der Beziehung geändert.

Weiterhin gibt es auch eine Implementierung in C (s. u.), die sehr schnell ist 
(<2 Mikrosekunden pro RSA-Moduli).

Wenn man nur wenige Schlüssel auf einmal testen will und einem Zeiten rund
einer Millisekunde nicht stören, kann man gleich den DLog-Fingerprint-Test
verwenden (und den Moduli-Residue-Test weglassen).

Ansonsten:

1. Zuerst wird mit dem Moduli-Residue-Test der RSA-Moduls getestet. Dieser Test
ist sehr schnell (<2 Mikrosekunden pro RSA-Moduli), hat aber eine
false-positive-rate von etwas weniger als 1/238878720. Falls man also unter
mehr als 4 Milliarden Schlüsseln mit diesem Test einen Kandidaten findet, so
sollte man auf diesen Kandidaten den folgenden, zweiten Test laufen lassen.

2. Der DLog-Fingerprint-Test benötigt ein wenig länger pro RSA-Schlüssel 
(<100 ms, als batch-job 0.1 ms) und hat eine false-positive-rate von unter 2^-154. 
(Diese FPR ist jenseits von Gut und Böse. Zum Vergleich bei einem FIPS-140-2 HSM
hört man bei der RSA-Schlüsselerzeugung bei 2^-112 mit den probabilistischen
Zufallstests auf.)


## Moduli-Residue-Test:

    $ cat mod01.txt
    0x944e13208a280c37efc31c3114485e590192adbb8e11c87cad60cdef0037ce99278330d3f471a2538fa667802ed2a3c44a8b7dea826e888d0aa341fd664f7fa7
    $ ./roca-test.py mod01.txt
    modulus=0x944e13208a280c37efc31c3114485e590192adbb8e11c87cad60cdef0037ce99278330d3f471a2538fa667802ed2a3c44a8b7dea826e888d0aa341fd664f7fa7
    Angreifbar

    $ ./x509der-to-pubkey-modulus.py GEM.TSL-CA1.der > test-modulus.txt
    $ ./roca-test.py test-modulus.txt
    modulus=0xe6d3e676388c37619c0495403f071625854f67ac726233518fd8e465689bea2a27c36a476d696a02dddd4ae505ed30efd41608ee498064e162db1d98c6e41ab0e79058f46e400a85ee7d1fd41a1fbd7f8dde6ac98f7f83997c1d8b3ba64446f5b3792efcab1135d431a55334a0cb1577729a33a5ac11ba8f1a80bb6a21125c1deb676c3e4d48e0652b1bbc127a3fb620b445919c556c6006ac785250fc1ee2d84159f82e4f93117d9375e8cd893a096db7d5faf6eeccf240f5951c62713a75ee8713e7e061ef4da3bb625a379dd7957c31e67f5ca68743adf9d1f519ed75ac1c8aae7bf5ca5092cb6f2e0068f7ed06907eb13e7c5a55d5fbd2c22dfb9a816c85
    OK

    $ time ./roca-test.py test-modulus.txt 
    modulus=0xe6d3e676388c37619c0495403f071625854f67ac726233518fd8e465689bea2a27c36a476d696a02dddd4ae505ed30efd41608ee498064e162db1d98c6e41ab0e79058f46e400a85ee7d1fd41a1fbd7f8dde6ac98f7f83997c1d8b3ba64446f5b3792efcab1135d431a55334a0cb1577729a33a5ac11ba8f1a80bb6a21125c1deb676c3e4d48e0652b1bbc127a3fb620b445919c556c6006ac785250fc1ee2d84159f82e4f93117d9375e8cd893a096db7d5faf6eeccf240f5951c62713a75ee8713e7e061ef4da3bb625a379dd7957c31e67f5ca68743adf9d1f519ed75ac1c8aae7bf5ca5092cb6f2e0068f7ed06907eb13e7c5a55d5fbd2c22dfb9a816c85
    OK

    real    0m0,050s
    user    0m0,036s
    sys     0m0,013s

    
    $ time ./dlog/DLogTest.py mod01.txt
    modulus=0x944e13208a280c37efc31c3114485e590192adbb8e11c87cad60cdef0037ce99278330d3f471a2538fa667802ed2a3c44a8b7dea826e888d0aa341fd664f7fa7
    Angreifbar

    real	0m0,109s
    user	0m0,062s
    sys	0m0,015s


### In C programmiert
Die python-Implementierung hat den Fokus auf leichte Lesbarkeit bzw. Verständlichkeit,
nicht Ausführungsgeschwindigkeit. Falls die Ausführungsgeschwindigkeit ein Problem
sein sollte, sollte man eine Implementierung in C wählen.

Im Verzeichnis [experimental/](experimental/) liegt die C-Implementierung in c_roca-test.c.
Dort das Programm kompilieren

    $ make
    gcc -O3 -o c_roca-test c_roca-test.c -lgmp
    gcc -O3 -o c_roca-test_2 c_roca-test_2.c -lgmp

Eine Millionen Testschlüssel erzeugen.

    $ make c_testschluessel.bin
    dd if=/dev/urandom bs=256 of=c_testschluessel.bin count=1000000
    1000000+0 Datensätze ein
    1000000+0 Datensätze aus
    256000000 bytes (256 MB, 244 MiB) copied, 5,43155 s, 47,1 MB/s
    $ ls -l c_testschluessel.bin
    -rw-r--r-- 1 a users 256000000 24. Okt 10:03 c_testschluessel.bin

Starten:

    $ time ./c_roca-test
    Testdateigröße: 256000000, Anzahl RSA-Moduli: 1000000
    Los geht\'s
    Fertig
    Benötigte Zeit: 1.812702000 Sekunden, Zeit (Sekunden) pro Element: 0.000001813
    bye

    real    0m1,824s
    user    0m1,796s
    sys     0m0,027s
    $ time ./c_roca-test_2
    Testdateigröße: 256000000, Anzahl RSA-Moduli: 1000000
    Los geht\'s
    Fertig
    Benötigte Zeit: 1.810442000 Sekunden, Zeit (Sekunden) pro Element: 0.000001810
    bye

    real    0m1,835s
    user    0m1,795s
    sys     0m0,037s


Interessant Weise hilft unter cygwin das Laden der Fingerprints per mpz_import
deutlich um die Startup-Kosten zu senken:

    $ time ./c_roca-test
    Testdateigröße: 256000000, Anzahl RSA-Moduli: 1000000
    Los geht\'s
    Fertig
    Benötigte Zeit: 1.856019000 Sekunden, Zeit (Sekunden) pro Element: 0.000001856
    bye

    real    0m1,926s
    user    0m1,803s
    sys     0m0,073s

    $ time ./c_roca-test_2
    Testdateigröße: 256000000, Anzahl RSA-Moduli: 1000000
    Los geht\'s
    Fertig
    Benötigte Zeit: 1.171536000 Sekunden, Zeit (Sekunden) pro Element: 0.000001172
    bye

    real    0m1,194s
    user    0m1,155s
    sys     0m0,037s


Einen Schlüssel zu testen kostet damit unter 2 Mikrosekunden.

Genau einen RSA-Moduls testen:

    $ dd if=/dev/urandom of=c_testschluessel.bin bs=256 count=1
    1+0 Datensätze ein
    1+0 Datensätze aus
    256 bytes copied, 0,00209225 s, 122 kB/s

    $ time ./c_roca-test_2
    Testdateigröße: 256, Anzahl RSA-Moduli: 1
    Los geht\'s
    Fertig
    Benötigte Zeit: 0.000037000 Sekunden, Zeit (Sekunden) pro Element: 0.000037000
    bye

    real    0m0,001s
    user    0m0,001s
    sys     0m0,000s

    $ time ./c_roca-test
    Testdateigröße: 256, Anzahl RSA-Moduli: 1
    Los geht\'s
    Fertig
    Benötigte Zeit: 0.000023000 Sekunden, Zeit (Sekunden) pro Element: 0.000023000
    bye

    real    0m0,001s
    user    0m0,001s
    sys     0m0,000s

Wir nehmen uns mal wieder eine Millionen Test-Moduli und fügen als letztes
Element im Datensatz eine ROCA-anfälligen RSA-Modulus hinzu:

    $ make c_testschluessel.bin
    dd if=/dev/urandom bs=256 of=c_testschluessel.bin count=1000000
    1000000+0 Datensätze ein 
    1000000+0 Datensätze aus 
    256000000 bytes (256 MB, 244 MiB) copied, 5,43155 s, 47,1 MB/s
    $ ./txt-to-bin.py ../mod03.txt >/dev/null
    $ ./c_roca-test; ./c_roca-test_2
    Testdateigröße: 256000256, Anzahl RSA-Moduli: 1000001
    Los geht\'s
    CRITICAL Schlüssel mit Index-Nummer 1000000 angreifbar (ROCA)
    Fertig
    Benötigte Zeit: 1.807269000 Sekunden, Zeit (Sekunden) pro Element: 0.000001807
    bye
    Testdateigröße: 256000256, Anzahl RSA-Moduli: 1000001
    Los geht\'s
    CRITICAL Schlüssel mit Index-Nummer 1000000 angreifbar (ROCA)
    Fertig
    Benötigte Zeit: 1.812120000 Sekunden, Zeit (Sekunden) pro Element: 0.000001812
    bye


## DLog-Fingerprint-Test

    [a@h experimental]$ pwd
    /home/a/git/roca/dlog/experimental
    [a@h experimental]$ rm -f primes.db
    [a@h experimental]$ time ./GenPrimesDBfromSeed 3163 a >/dev/null

    real    6m29,806s
    user    6m29,423s
    sys     0m0,030s
    [a@h experimental]$
    [a@h experimental]$ ./roca-residu-test.py
    Ich habe primes.db (size=404864), damit Elemente=3163.
    Ich teste 10001406 Moduli ((log10) 7.000061057511913).
    100% Time:  0:01:23 116.5 KiB/s |###############################|

Damit werden pro Schlüssel 8.3 Mikrosekunden benötigt für den ROCA-Restwert-Test (in Python implementiert).

    [a@h experimental]$ ./roca-DLogTest.py
    Ich habe primes.db (size=404864), damit Elemente=3163.
    Ich teste 10001406 Moduli ((log10) 7.000061057511913).
    100% Time:  0:22:42   7.2 KiB/s |###############################|


Damit werden pro Schlüssel 136.3 Mikrosekunden benötigt für den DLog-Fingerprint--Test (in Python implementiert).
D. h., 0.1 Milisekunden pro Schlüssel.


Linux PC:

    [a@h roca]$ time dlog/DLogTest.py mod01.txt 
    modulus=0x944e13208a280c37efc31c3114485e590192adbb8e11c87cad60cdef0037ce99278330d3f471a2538fa667802ed2a3c44a8b7dea826e888d0aa341fd664f7fa7
    Angreifbar

    real    0m0,060s
    user    0m0,060s
    sys     0m0,000s

Windows Laptop cygwin:

    $ time ./dlog/DLogTest.py mod01.txt
    modulus=0x944e13208a280c37efc31c3114485e590192adbb8e11c87cad60cdef0037ce99278330d3f471a2538fa667802ed2a3c44a8b7dea826e888d0aa341fd664f7fa7
    Angreifbar

    real	0m0,203s
    user	0m0,062s
    sys	0m0,108s

    $ cd dlog/
    $ for i in false-positives-residue-test/mod0* ; do echo $i; echo Residue-Test:; ../roca-test.py $i; echo DLog-Test:; ./DLogTest.py $i; done | grep -v modu
    false-positives-residue-test/mod01.txt
    Residue-Test:
    Angreifbar
    DLog-Test:
    OK
    false-positives-residue-test/mod02.txt
    Residue-Test:
    Angreifbar
    DLog-Test:
    OK
    false-positives-residue-test/mod03.txt
    Residue-Test:
    Angreifbar
    DLog-Test:
    OK
    false-positives-residue-test/mod04.txt
    Residue-Test:
    Angreifbar
    DLog-Test:
    OK

Windows Laptop cygwin:

    $ for i in false-positives-residue-test/mod0* ; do echo $i; echo; time ../roca-test.py $i; time ./DLogTest.py $i; echo ;done
    false-positives-residue-test/mod01.txt

    modulus=0x574f0be87b64c72207762d918c0b1bacc28f97441fb9f1f65da6b9edfbc47d399e46060a6ced758ebbd1d64843f1dcd6a7155fbe5657a1e1faba72cd1983107f48636cd356a666f90a87279b6058fab337ea1d80ce759f48c62879de766dcd1f7758430370a8433b01aec5c2e2e058d0becc80135cc1f783db74cde427b8302875c024d4156067c49084fb6a97781205ffdd4ee2554021d2ce69b99fc2d6d536e0ded9476abbbafcd85f9783b7ad4896826e68fc1fb14c8bc47c30e64324554bed89968796124dc6626089cf597b1a68d0a82b6720633cb250be4e3c092cbb8673f910efa21c8790a438821e9f1bed77ef61787230cae9b502f19dffd1a74c49
    Angreifbar

    real    0m0,156s
    user    0m0,062s
    sys     0m0,045s
    modulus=0x574f0be87b64c72207762d918c0b1bacc28f97441fb9f1f65da6b9edfbc47d399e46060a6ced758ebbd1d64843f1dcd6a7155fbe5657a1e1faba72cd1983107f48636cd356a666f90a87279b6058fab337ea1d80ce759f48c62879de766dcd1f7758430370a8433b01aec5c2e2e058d0becc80135cc1f783db74cde427b8302875c024d4156067c49084fb6a97781205ffdd4ee2554021d2ce69b99fc2d6d536e0ded9476abbbafcd85f9783b7ad4896826e68fc1fb14c8bc47c30e64324554bed89968796124dc6626089cf597b1a68d0a82b6720633cb250be4e3c092cbb8673f910efa21c8790a438821e9f1bed77ef61787230cae9b502f19dffd1a74c49
    OK

    real    0m0,094s
    user    0m0,078s
    sys     0m0,000s

    false-positives-residue-test/mod02.txt

    modulus=0x70c84d9692837bc38f0525191f40c6fdc8524f7bb1e9a500c53ea93a1e6b53dc7f4d888c3c036c1651dbaa2485c02243d11d31d9c4130c57c0fca3233e8e9779564d7a7b6c9c65e777bde1baadd0bca6a92fe6111e378ee66ed9a3c1aa88c92d9ed7a538bc5493ee07b5e2b563733f4a08dea577a04280daf237ae0ececfee4fb367bcf98189b6da3489da5a1a458ab9725127d7de70447cd735c55d7845839e6b028f252e9b95a1955a238fb2fe8405cc78940dbe2c93b7e09491a025beb1c1418d267fd4bacec1d63fe9e2641670d19886b36fcd904f47188f366c00ee87605f54c04e8c68be16dba7107fcb64320a1b1fd5f56f5961eacd04fee749eea8a9
    Angreifbar

    real    0m0,078s
    user    0m0,046s
    sys     0m0,015s
    modulus=0x70c84d9692837bc38f0525191f40c6fdc8524f7bb1e9a500c53ea93a1e6b53dc7f4d888c3c036c1651dbaa2485c02243d11d31d9c4130c57c0fca3233e8e9779564d7a7b6c9c65e777bde1baadd0bca6a92fe6111e378ee66ed9a3c1aa88c92d9ed7a538bc5493ee07b5e2b563733f4a08dea577a04280daf237ae0ececfee4fb367bcf98189b6da3489da5a1a458ab9725127d7de70447cd735c55d7845839e6b028f252e9b95a1955a238fb2fe8405cc78940dbe2c93b7e09491a025beb1c1418d267fd4bacec1d63fe9e2641670d19886b36fcd904f47188f366c00ee87605f54c04e8c68be16dba7107fcb64320a1b1fd5f56f5961eacd04fee749eea8a9
    OK

    real    0m0,109s
    user    0m0,046s
    sys     0m0,046s

    false-positives-residue-test/mod03.txt

    modulus=0xa0eb65317d055df25818d29d0c1751672892d020ceb46adb9376b18ff7c3a15a866c31f8e5e021a4d5dc4db628d143558cf275592ec6494a0259fba6bd75ad26e03939485c1f87912ed03fa11c6b63d9c0698b42c295086cac1ac98229d39fc376283f37b30488ddc5a8959210900423d263edd8580f250b97d262a65740e8c6662b4a17e71fdad40d3e41500bcc4d264174158967e9764d0da21ae9167aa2118da586dd753a5dc161aabfc4ea819676af34c2e0ed6f997af7640abc5eb202784b054a5bd3c005613d73ad5fc3c557b5d813a795a2ec08529769c79497161a2a6aab54b1b5374eca309a2cf5f1d10d2ec036b714c0b1dd158e66d90f14852383
    Angreifbar

    real    0m0,078s
    user    0m0,031s
    sys     0m0,015s
    modulus=0xa0eb65317d055df25818d29d0c1751672892d020ceb46adb9376b18ff7c3a15a866c31f8e5e021a4d5dc4db628d143558cf275592ec6494a0259fba6bd75ad26e03939485c1f87912ed03fa11c6b63d9c0698b42c295086cac1ac98229d39fc376283f37b30488ddc5a8959210900423d263edd8580f250b97d262a65740e8c6662b4a17e71fdad40d3e41500bcc4d264174158967e9764d0da21ae9167aa2118da586dd753a5dc161aabfc4ea819676af34c2e0ed6f997af7640abc5eb202784b054a5bd3c005613d73ad5fc3c557b5d813a795a2ec08529769c79497161a2a6aab54b1b5374eca309a2cf5f1d10d2ec036b714c0b1dd158e66d90f14852383
    OK

    real    0m0,094s
    user    0m0,046s
    sys     0m0,030s

    false-positives-residue-test/mod04.txt

    modulus=0xbf26a520694801164bc2c335ca157671798e6088c55d7c957ca575d1fafe861180e59a6ef93a7056ddf78c7a458ee3c77e720fccbc17dcb17a83109178362d09024b1a8f29f4a805905ddfda036c2db19713d51a217e4cb2b3b88a3e7e47f160fb02609dad92ea3a843898c79540b003569ed76b9337cce40ffc4f412a9c48d971cb35087b95717cede1c176efce2ac9a5ab56d7cbd4736fdc399b2f0920e3bad0b29c0806fa0e00b5a4f76db097400f39178527672c93c7181cb77b2e3e873150012a062d65ed45234a4287d7a4d8a85de955e168e989fd737a6af9da9780829f8baefe6a9445a2e5119449f7545652f19596e2df665a93328c2b0cf8911385
    Angreifbar

    real    0m0,109s
    user    0m0,062s
    sys     0m0,031s
    modulus=0xbf26a520694801164bc2c335ca157671798e6088c55d7c957ca575d1fafe861180e59a6ef93a7056ddf78c7a458ee3c77e720fccbc17dcb17a83109178362d09024b1a8f29f4a805905ddfda036c2db19713d51a217e4cb2b3b88a3e7e47f160fb02609dad92ea3a843898c79540b003569ed76b9337cce40ffc4f412a9c48d971cb35087b95717cede1c176efce2ac9a5ab56d7cbd4736fdc399b2f0920e3bad0b29c0806fa0e00b5a4f76db097400f39178527672c93c7181cb77b2e3e873150012a062d65ed45234a4287d7a4d8a85de955e168e989fd737a6af9da9780829f8baefe6a9445a2e5119449f7545652f19596e2df665a93328c2b0cf8911385
    OK

    real    0m0,094s
    user    0m0,046s
    sys     0m0,031s


