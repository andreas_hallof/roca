Nur so aus interessene die Laufzeiten einer (einfachen) virtuellen Serverinstanz:

	a@t:~/git/roca/experimental$ ./c_roca-test
	Testdateigröße: 256000000, Anzahl RSA-Moduli: 1000000
	Los geht's
	Fertig
	Benötigte Zeit: 1.180885000 Sekunden, Zeit (Sekunden) pro Element: 0.000001181
	bye
	a@t:~/git/roca/experimental$ ./c_roca-test_2
	Testdateigröße: 256000000, Anzahl RSA-Moduli: 1000000
	Los geht's
	Fertig
	Benötigte Zeit: 0.846505000 Sekunden, Zeit (Sekunden) pro Element: 0.000000847
	bye
	a@t:~/git/roca/experimental$ openssl speed rsa2048
	Doing 2048 bit private rsa's for 10s: 10323 2048 bit private RSA's in 9.99s
	Doing 2048 bit public rsa's for 10s: 355935 2048 bit public RSA's in 9.98s
	OpenSSL 1.1.0g  2 Nov 2017
	built on: reproducible build, date unspecified
	options:bn(64,64) rc4(16x,int) des(int) aes(partial) blowfish(ptr)
	compiler: gcc -DDSO_DLFCN -DHAVE_DLFCN_H -DNDEBUG -DOPENSSL_THREADS -DOPENSSL_NO_STATIC_ENGINE -DOPENSSL_PIC -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DRC4_ASM -DMD5_ASM -DAES_ASM -DVPAES_ASM -DBSAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DPADLOCK_ASM -DPOLY1305_ASM -DOPENSSLDIR="\"/usr/lib/ssl\"" -DENGINESDIR="\"/usr/lib/x86_64-linux-gnu/engines-1.1\""
			  sign    verify    sign/s verify/s
	rsa 2048 bits 0.000968s 0.000028s   1033.3  35664.8

Interessanter Weise ist auf Windows (cywin) die mpz_import-Variante beim Startup deutlich schneller

	$ ./c_roca-test ; ./c_roca-test_2
	Testdateigröße: 256000000, Anzahl RSA-Moduli: 1000000
	Los geht's
	Fertig
	Benötigte Zeit: 1.342000000 Sekunden, Zeit (Sekunden) pro Element: 0.000001342
	bye
	Testdateigröße: 256000000, Anzahl RSA-Moduli: 1000000
	Los geht's
	Fertig
	Benötigte Zeit: 0.951000000 Sekunden, Zeit (Sekunden) pro Element: 0.000000951
	bye
	
Das scheint unter Linux kaum einen Unterschied zu machen.


