#! /usr/bin/env python3

import sys, math

def bytes_needed(n):
    if n == 0:
        return 1
    return int(math.log(n, 256)) + 1

if len(sys.argv)<2:
    sys.exit("Ich erwarte als erstes Argument den Dateinamen einer Textdatei"+
             "mit dem RSA-Modulus im Hexadezimalformat.");

with open(sys.argv[1], "rt") as f_in:
    for data in f_in:
        modulus=int(data,16)
        print(hex(modulus))
        with open("c_testschluessel.bin", "ab") as f:
            f.write(modulus.to_bytes(bytes_needed(modulus),
                                     byteorder='big',
                                     signed=False
                                    )
                   )

