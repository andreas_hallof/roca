#include <stdio.h>
#include <gmp.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <assert.h>
#include <time.h>


/* https://techoverflow.net/2013/08/21/a-simple-mmap-readonly-example/
*/
size_t getFilesize(const char* filename)
{
    struct stat st;
    stat(filename, &st);
    return st.st_size;
}

#define Testschluessel "c_testschluessel.bin"

int main(int argc, char** argv) 
{
    clock_t zeit;
    int Angreifbar;

    mpz_t primes[38], prints[38], res1, res2, modulus, exponent, eins;
    unsigned long int  primes_val[]={ 
        3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67,
        71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139,
        149, 151, 157, 163, 167 };

    char * prints_str[] = { 
          "0x6",
          "0x1e",
          "0x7e",
          "0x402",
          "0x161a",
          "0x1a316",
          "0x30af2",
          "0x7ffffe",
          "0x1ffffffe",
          "0x7ffffffe",
          "0x4000402",
          "0x1fffffffffe",
          "0x7fffffffffe",
          "0x7ffffffffffe",
          "0x12dd703303aed2",
          "0x7fffffffffffffe",
          "0x1434026619900b0a",
          "0x7fffffffffffffffe",
          "0x1164729716b1d977e",
          "0x147811a48004962078a",
          "0xb4010404000640502",
          "0x7fffffffffffffffffffe",
          "0x1fffffffffffffffffffffe",
          "0x1000000006000001800000002",
          "0x1ffffffffffffffffffffffffe",
          "0x16380e9115bd964257768fe396",
          "0x27816ea9821633397be6a897e1a",
          "0x1752639f4e85b003685cbe7192ba",
          "0x1fffffffffffffffffffffffffffe",
          "0x6ca09850c2813205a04c81430a190536",
          "0x7fffffffffffffffffffffffffffffffe",
          "0x1fffffffffffffffffffffffffffffffffe",
          "0x7fffffffffffffffffffffffffffffffffe",
          "0x1ffffffffffffffffffffffffffffffffffffe",
          "0x50c018bc00482458dac35b1a2412003d18030a",
          "0x161fb414d76af63826461899071bd5baca0b7e1a",
          "0x7fffffffffffffffffffffffffffffffffffffffe",
          "0x7ffffffffffffffffffffffffffffffffffffffffe"};

    for (int i=0; i<38; i++) {
      mpz_init_set_ui(primes[i],primes_val[i]);
      // gmp_printf("%d %d %Zd\n", i, primes_val[i], primes[i]);
      mpz_init_set_str(prints[i], prints_str[i], 0);
      // gmp_printf("%Zd\n", prints[i]);
    }
    mpz_init_set_ui(eins, 1);

    size_t filesize = getFilesize(Testschluessel);
    int fd = open(Testschluessel, O_RDONLY, 0); assert(fd != -1);
    const char * mmappedData = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0); assert(mmappedData != MAP_FAILED);
    unsigned long int Elemente=filesize>>8;
    printf("Testdateigröße: %ld, Anzahl RSA-Moduli: %ld\n", filesize, Elemente);

    printf("Los geht's\n");
    /* https://www.gnu.org/software/libc/manual/html_node/CPU-Time.html
     */
    zeit=clock();

    mpz_inits(modulus, exponent, res1, res2, NULL);

    /* nichttriviales Verhältnis zwischen int und long
     * https://stackoverflow.com/questions/18971732/what-is-the-difference-between-long-long-long-long-int-and-long-long-i
     * https://en.wikipedia.org/wiki/64-bit_computing#64-bit_data_models
     */
    for (long int i=0; i<Elemente; i++)
    {
        Angreifbar=1;
        mpz_import(modulus, 256, 1, 1, 0, 0, &mmappedData[i<<8]);

        for (int x=0; x<38; x++)
        {
            mpz_mod(exponent, modulus, primes[x]);
            //gmp_printf("Exponent %Zd\n", exponent);
            mpz_mul_2exp(res1, eins, mpz_get_ui(exponent));
            // res1=1 << (modulus % primes[i])
            mpz_and(res2, res1, prints[x]);
            if (mpz_cmp_ui(res2,0)==0)
            {
                Angreifbar=0;
                break;
            }

        }

        if (Angreifbar>0) 
        {
            printf("CRITICAL Schlüssel mit Index-Nummer %ld angreifbar (ROCA)\n", i);

        }

    }
  
    
    zeit=clock() - zeit;
    double zeit_diff= ( (double) zeit) / CLOCKS_PER_SEC;
    printf("Fertig\n");

    printf("Benötigte Zeit: %.9f Sekunden, Zeit (Sekunden) pro Element: %.9f\n", zeit_diff, zeit_diff / ((double) Elemente));

    close(fd);

    printf("bye\n");

    return 0;
}

