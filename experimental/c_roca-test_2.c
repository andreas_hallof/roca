#include <stdio.h>
#include <gmp.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <assert.h>
#include <time.h>


/* https://techoverflow.net/2013/08/21/a-simple-mmap-readonly-example/
*/
size_t getFilesize(const char* filename)
{
    struct stat st;
    stat(filename, &st);
    return st.st_size;
}

#define Testschluessel "c_testschluessel.bin"

int main(int argc, char** argv) 
{
    clock_t zeit;
    int Angreifbar;

    mpz_t primes[38], prints[38], res1, res2, modulus, exponent, eins;
    unsigned long int  primes_val[]={ 
        3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67,
        71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139,
        149, 151, 157, 163, 167 };

#define FingerprintDBName "fingerprints_db.bin"
#define FingerprintDBSize 798
#define FingerprintLen 21
    char DBData[FingerprintDBSize];
    int fd_db=open(FingerprintDBName, O_RDONLY);
    assert(read(fd_db, DBData, FingerprintDBSize)==FingerprintDBSize);

    for (int i=0; i<38; i++) {
      mpz_init_set_ui(primes[i],primes_val[i]);
      mpz_init(prints[i]);
      /* void mpz_import (mpz_t rop, size_t count, int order, size_t size, int endian, size_t nails, const void *op) */
      mpz_import(         prints[i], FingerprintLen, 1,          1,            1,          0,          &DBData[i*FingerprintLen]);
      // gmp_printf("%d: %Zd\n", i, prints[i]);
    }
    mpz_init_set_ui(eins, 1);


    size_t filesize = getFilesize(Testschluessel);
    int fd = open(Testschluessel, O_RDONLY, 0); assert(fd != -1);
    const char * mmappedData = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0); assert(mmappedData != MAP_FAILED);
    unsigned long int Elemente=filesize>>8;
    printf("Testdateigröße: %ld, Anzahl RSA-Moduli: %ld\n", filesize, Elemente);

    printf("Los geht's\n");
    /* https://www.gnu.org/software/libc/manual/html_node/CPU-Time.html
     */
    zeit=clock();

    mpz_inits(modulus, exponent, res1, res2, NULL);

    /* nichttriviales Verhältnis zwischen int und long
     * https://stackoverflow.com/questions/18971732/what-is-the-difference-between-long-long-long-long-int-and-long-long-i
     * https://en.wikipedia.org/wiki/64-bit_computing#64-bit_data_models
     */
    for (int i=0; i<Elemente; i++)
    {
        Angreifbar=1;
        mpz_import(modulus, 256, 1, 1, 1, 0, &mmappedData[i<<8]);

        for (int x=0; x<38; x++)
        {
            mpz_mod(exponent, modulus, primes[x]);
            //gmp_printf("Exponent %Zd\n", exponent);
            mpz_mul_2exp(res1, eins, mpz_get_ui(exponent));
            // res1=1 << (modulus % primes[i])
            mpz_and(res2, res1, prints[x]);
            if (mpz_cmp_ui(res2,0)==0)
            {
                Angreifbar=0;
                break;
            }

        }

        if (Angreifbar>0) 
        {
            printf("CRITICAL Schlüssel mit Index-Nummer %ld angreifbar (ROCA)\n", i);

        }

    }
  
    
    zeit=clock() - zeit;
    double zeit_diff= ( (double) zeit) / CLOCKS_PER_SEC;
    printf("Fertig\n");

    printf("Benötigte Zeit: %.9f Sekunden, Zeit (Sekunden) pro Element: %.9f\n", zeit_diff, zeit_diff / ((double) Elemente));

    close(fd);

    printf("bye\n");

    return 0;
}

