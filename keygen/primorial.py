#!/usr/bin/env python3

from random import randint, choice
from gmpy2 import is_prime, next_prime
from math import log2

def get_primorial(n : int):
    assert n>1

    p=1; M=1

    for i in range(0,n):
        p=next_prime(p)
        print(p)
        M*=p

    return M

M_39=get_primorial(39)
M_126=get_primorial(126)

print("M_39=0x{:x} {:f}".format(M_39, log2(M_39)))
print("M_126=0x{:x} {:f}".format(M_126, log2(M_126)))

