#!/usr/bin/env python3

from random import randint, choice
from gmpy2 import is_prime 
from secrets import randbits
from math import log2

# see primorial.py
M_126=0x7cda79f57f60a9b65478052f383ad7dadb714b4f4ac069997c7ff23d34d075fca08fdf20f95fbc5f0a981d65c3a3ee7ff74d769da52e948d6b0270dd736ef61fa99a54f80fb22091b055885dc22b9f17562778dfb2aeac87f51de339f71731d207c0af3244d35129feba028a48402247f4ba1d2b6d0755baff6

def get_prime():
    k=randbits(54)
    a=randbits(32)

    return k*M_126 + pow(65537, a, M_126)

p = get_prime()
q = get_prime()
N = p * q

print("p = 0x{:x}".format(p), is_prime(p))
print("q = 0x{:x}".format(q), is_prime(q))
print("N = 0x{:x} ({})".format(
        N, 
        log2(N)
        )
     )
#print("Vulnerable according to tester:", vulnerable(N))

