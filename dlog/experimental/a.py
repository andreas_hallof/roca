#! /usr/bin/env python3

from math import log2, log10
from gmpy2 import next_prime

primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167]

print(len(primes))

prod=1
for i in primes:
    prod*=(i-1)
print(prod, log2(prod), log10(prod))

p2 = primes
while len(p2)<126:
    p2.append(next_prime(p2[-1]))
print(p2)

prod=1
for i in p2:
    prod*=(i-1)
print(prod, log2(prod), log10(prod))

