#include <stdio.h>
#include <gmp.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include <openssl/sha.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>




#define PRIMES_DB "primes.db"
int main(int argc, char** argv) {

    if (argc<3) {
        printf("Usage: %s NumberOf1024BitPrimes DerivationSeed\nThe \"DerivationSeed\" can be an abritary string.\n", argv[0]);
        return 1;
    }

    /* https://stackoverflow.com/questions/18971732/what-is-the-difference-between-long-long-long-long-int-and-long-long-i 
    *  https://de.wikipedia.org/wiki/Datentypen_in_C
    */
    unsigned long NumberOfPrimes = atol(argv[1]);
    assert(sizeof(NumberOfPrimes)==8);
    printf("Appending %ld (1024 bit) primes to \"%s\" (%ld KiB / %.2f MiB).\n", 
        NumberOfPrimes, PRIMES_DB, NumberOfPrimes >> 3, (double) (NumberOfPrimes >> 3)/1024.0
    );
    
    int primes_fd = open(PRIMES_DB, O_WRONLY | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if (primes_fd==-1) {
        perror("upps"); exit(1);
    }

    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, argv[2], strlen(argv[2]));
    SHA256_Final(hash, &sha256);

    unsigned char InfoText[65]={'\x00'};

    int i_info = 0;
    for (i_info = 0; i_info < 16; i_info++)
    {
        sprintf(&InfoText[i_info * 2], "%02x", hash[i_info]);
    }
    
    printf("Using AES-Key \"%s\" as derivation key.\n", InfoText);

    EVP_CIPHER_CTX *ctx;

    if (!(ctx = EVP_CIPHER_CTX_new())) {
        perror("upps"); exit(1);
    }
    if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, hash, NULL)) {
        perror("upps"); exit(1);
    }


    int ciphertext_len;
    unsigned char plaintext[128]={0}, ciphertext[128];
    clock_t zeit, zeit_t1;
    double zeit_diff;
    mpz_t p, ZweiHoch1024, ZweiHoch1023, r;
    mpz_inits(p, ZweiHoch1024, ZweiHoch1023, r, NULL);
    mpz_ui_pow_ui(ZweiHoch1024, 2, 1024);
    mpz_ui_pow_ui(ZweiHoch1023, 2, 1023);

    char p_export[128];
    size_t countp, res_write;

    /* https://www.gnu.org/software/libc/manual/html_node/CPU-Time.html
     */
    zeit=clock();

    unsigned long sechstenstell=NumberOfPrimes >> 4,
         proc_counter=1, 
         proc_countdown=sechstenstell;

    unsigned long retry_counter=0;

    assert(sizeof(proc_counter==8));

    //assert(sechstenstell>0);

    printf("Starting calculation ...\n");
    for (unsigned long i=0; i<NumberOfPrimes; i++) {
	neustart:
	//printf("Zahl %ld retry_counter %ld\n", i, retry_counter);
        memcpy(plaintext, &i, sizeof(i));
	memcpy(plaintext+8, &retry_counter, sizeof(retry_counter));
        if (1 != EVP_EncryptUpdate(ctx, ciphertext, &ciphertext_len, plaintext, 128 /*plaintext_len*/)) {
            perror("upps"); exit(1);
        }
        assert(ciphertext_len==128);
        /* https://en.wikipedia.org/wiki/Bitwise_operations_in_C#Bitwise_OR_| 
        */
        ciphertext[0]=ciphertext[0] | 128;
        ciphertext[127]=ciphertext[127] | 1;
	/*
	for (int tmp=0; tmp<127; tmp++) {
		printf("%0x", ciphertext[tmp]);
	}
	printf("\n");
	sleep(10);
	*/

        mpz_import(p, 128, 1, 1, 1, 0, &ciphertext);

#define LE(a,b) (mpz_cmp(a,b)<=0)

	if (LE(p, ZweiHoch1023)) {
		printf("habe erhöht\n");
		mpz_sub(r, ZweiHoch1023, p);
		gmp_printf("Diff %Zd\n", r);
		mpz_add(p, ZweiHoch1023, p);
	}

	/*
        gmp_printf("Zahl %Zd\n", p);
	sleep(1);
	*/

	if (LE(ZweiHoch1024, p)) {
		printf("Fehler schon hier zu gross.\n");
		mpz_sub(r, p, ZweiHoch1024);
		gmp_printf("obere Schranke %Zd\n", ZweiHoch1024);
		gmp_printf("Zahl           %Zd\n", p);
		gmp_printf("Diff           %Zd\n", r);
		retry_counter++;
		goto neustart;
	}


        mpz_nextprime(p, p);
        // gmp_printf("Zahl %Zd\n", p);
        /* Function: void * mpz_export (
         * void *rop, size_t *countp, int order, size_t size, int endian, size_t nails,
         * const mpz_t op) */

	if (LE(ZweiHoch1024, p)) {
		printf("Fehler zu gross.\n");
		mpz_sub(r, p, ZweiHoch1024);
		gmp_printf("diff %Zd\n", r);
		retry_counter++;
		goto neustart;
	}
	if (LE(p, ZweiHoch1023)) {
		printf("Fehler zu klein.\n");
		gmp_printf("untere Schranke %Zd\n", ZweiHoch1023);
		gmp_printf("Zahl            %Zd\n", p);
		mpz_sub(r, ZweiHoch1023, p);
		gmp_printf("diff            %Zd\n", r);
		retry_counter++;
		goto neustart;
	}

	//printf("Zahl OK\n");

        mpz_export(&p_export, &countp, 1, 1, 1, 0, p);
	assert(countp==128);
	mpz_import(r, 128, 1, 1, 1, 0, &p_export);
	assert(mpz_cmp(r,p)==0);
	assert(mpz_probab_prime_p(p, 50)>0);

        res_write=write(primes_fd, p_export, countp);
        if (res_write == -1) {
            perror("upps"); exit(1);
        }
        /*
        printf("%d\n", res_write);
        assert(res_write==countp);
        */
        // printf("hh %d %d\n", proc_counter, proc_countdown);
        if (proc_countdown==0) {

            proc_counter++;
            proc_countdown=sechstenstell;
        
            double p=(double) i/NumberOfPrimes;
            printf("currently at i=%ld target=%ld (%2.0f%%).", i, NumberOfPrimes, 100.0*p);
            zeit_t1=clock() - zeit;
            zeit_diff= (( (double) zeit_t1) / CLOCKS_PER_SEC )*(1-p)/p;
            printf(" ETA %fd %fh, %fm, %fs\n", zeit_diff/(3600.0*24.0), zeit_diff/3600.0, zeit_diff/60.0, zeit_diff);
	    printf(" Retry_counter=%ld\n", retry_counter);
        }
        proc_countdown--;
    }

    close(primes_fd);

    zeit=clock() - zeit;
    zeit_diff= ( (double) zeit) / CLOCKS_PER_SEC;
    printf("used time: %.9f s, seconds per prime: %.9f\n", zeit_diff, zeit_diff / ((double) NumberOfPrimes));
    if (zeit_diff>60) {
        printf("(Umrechnungshilfe: Insgesamt %.9f Minuten bzw. %.9f Stunden)\n",
                zeit_diff/60.0, zeit_diff/(3600.0) );
    }

    return 0;
}

