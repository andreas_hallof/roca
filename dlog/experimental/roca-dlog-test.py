#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import math, pyfiglet, logging, os, sys, progressbar
from KeyGenUtils import PrimeDB
#sys.path.append( os.path.join(os.getcwd(), os.pardir, 'ent') )
sys.path.append( os.pardir )
from DLogTest import DLogFprint

db=PrimeDB("primes.db"); print(db)
Anz=db.getNumberOfElements()
#Anz=1000

tt=Anz**2-Anz
s="Ich teste {} Moduli ((log10) {}).".format(tt, math.log10(tt))
print(s); logging.info(s)

dlogtest=DLogFprint()

widgets = [progressbar.Percentage(), ' ', progressbar.ETA(), ' ', progressbar.AdaptiveTransferSpeed(), ' ', progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=(Anz)**2).start()
for a in range(0, Anz):
    for b in range(0, Anz):
        if a!=b:
            n=db.get(a)*db.get(b)
            if dlogtest.fprint(n):
                s="ups gefunden n={:x}, p={:x}, q={:x}, a={:x}, b={:x}".format(n,
                            db.get(a), db.get(b), a, b)
                print(s); logging.info(s)

        #bar.update(a*Anz + b)
    bar.update(a*Anz)
bar.finish()

logging.info("Run (Anz={:x}) zu Ende.".format(Anz))

