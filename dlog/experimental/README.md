merker Laufzeit Laptop (cygwin)

    $ time ./GenPrimesDBfromSeed.exe 3163 a
    Appending 3163 (1024 bit) primes to "primes.db" (395 KiB / 0.39 MiB).
    Using AES-Key "ca978112ca1bbdcafac231b39a23dc4d" as derivation key.
    Starting calculation ...
    currently at i=197 target=3163 ( 6%). ETA 0.002052d 0.049258h, 2.955461m, 177.327655s
     Retry_counter=0
    currently at i=394 target=3163 (12%). ETA 0.001995d 0.047874h, 2.872428m, 172.345652s
     Retry_counter=0
    currently at i=591 target=3163 (19%). ETA 0.001843d 0.044241h, 2.654469m, 159.268162s
     Retry_counter=0
    currently at i=788 target=3163 (25%). ETA 0.001694d 0.040657h, 2.439448m, 146.366910s
     Retry_counter=0
    currently at i=985 target=3163 (31%). ETA 0.001554d 0.037302h, 2.238107m, 134.286414s
     Retry_counter=0
    currently at i=1182 target=3163 (37%). ETA 0.001409d 0.033822h, 2.029296m, 121.757757s
     Retry_counter=0
    currently at i=1379 target=3163 (44%). ETA 0.001265d 0.030368h, 1.822056m, 109.323365s
     Retry_counter=0
    currently at i=1576 target=3163 (50%). ETA 0.001122d 0.026932h, 1.615917m, 96.955026s
     Retry_counter=0
    currently at i=1773 target=3163 (56%). ETA 0.000981d 0.023546h, 1.412788m, 84.767265s
     Retry_counter=0
    currently at i=1970 target=3163 (62%). ETA 0.000843d 0.020222h, 1.213317m, 72.799040s
     Retry_counter=0
    currently at i=2167 target=3163 (69%). ETA 0.000703d 0.016864h, 1.011819m, 60.709119s
     Retry_counter=0
    currently at i=2364 target=3163 (75%). ETA 0.000563d 0.013507h, 0.810396m, 48.623747s
     Retry_counter=0
    currently at i=2561 target=3163 (81%). ETA 0.000424d 0.010183h, 0.610983m, 36.659003s
     Retry_counter=0
    currently at i=2758 target=3163 (87%). ETA 0.000286d 0.006867h, 0.412000m, 24.719978s
     Retry_counter=0
    currently at i=2955 target=3163 (93%). ETA 0.000147d 0.003525h, 0.211525m, 12.691519s
     Retry_counter=0
    currently at i=3152 target=3163 (100%). ETA 0.000008d 0.000187h, 0.011196m, 0.671754s
     Retry_counter=0
    used time: 193.175000000 s, seconds per prime: 0.061073348
    (Umrechnungshilfe: Insgesamt 3.219583333 Minuten bzw. 0.053659722 Stunden)

    real    3m13,534s
    user    3m13,144s
    sys     0m0,061s

    $ ./roca-residu-test.py
    Ich habe primes.db (size=404864), damit Elemente=3163.
    Ich teste 10001406 Moduli ((log10) 7.000061057511913).
    100% Time: 0:01:17 126.6 KiB/s |#####################|

    $ ./roca-dlog-test.py
    Ich habe primes.db (size=404864), damit Elemente=3163.
    Ich teste 10001406 Moduli ((log10) 7.000061057511913).
    100% Time: 0:20:18   8.0 KiB/s |#####################|


