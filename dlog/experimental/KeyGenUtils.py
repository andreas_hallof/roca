#! /usr/bin/env python3

import secrets, math, sys, pyfiglet, shutil, base64, progressbar, \
       platform, resource, mmap
from hashlib import sha256
from binascii import hexlify
from gmpy2 import next_prime, is_prime
from collections import Counter

import os, sys; 
#sys.path.append( os.path.join(os.getcwd(), os.pardir, 'ent') )

#https://www.programcreek.com/python/example/2580/resource.getrusage
def MemUsage():
    return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

def bytes_needed(n):
    if n == 0:
        return 1
    return int(math.log(n, 256)) + 1

def i2osp(integer, size=4):
  return ''.join([chr((integer >> (8 * i)) & 0xFF) for i in reversed(range(size))])

def banner(s):
    if not s:
        return 
    #rows, columns = os.popen('stty size', 'r').read().split()
    x=shutil.get_terminal_size((80, 24))
    print(pyfiglet.Figlet(font='cybermedium', width=int(x.columns)).renderText(s))

# experimenting with the entropy-measure-function from
# https://rosettacode.org/wiki/Entropy#Python
def entropy(s):
    p, lns = Counter(s), float(len(s))
    return -sum( count/lns * math.log(count/lns, 2) for count in p.values())

class SmallPrimeFactorTest:
    def __init__(self, Total=-1, B=100):
        if Total==-1:
            raise ValueError("you have to supply a 'Total'-value")
        self.Total=Total
        self.B=B
        primes=[2, 3, 5, 7, 11, 13]
        while primes[-1]<B:
            x=next_prime(primes[-1])
            primes.append(x)
        self.primes=primes[:-1]
        self.cnt=Counter()

    def test(self, n):
        for p in self.primes:
            if n%p==0:
                self.cnt[p]+=1
                return True
        return False

    def __str__(self):
        return "Ich teste die ersten {} Primzahlen (max={}).".format(
            len(self.primes), self.primes[-1])

    def stats(self):
        res=""
        for (p,n) in self.cnt.most_common():
            res+="{}: {} ({:3.6f}%); ".format(p, n, 100*n/float(self.Total))

        return res

    def PrimesStr(self):
        return "[ "+", ".join(str(i) for i in self.primes)+" ]"

    def MissingPrimesStr(self):
        res=""
        for i in self.primes:
            if not i in self.cnt:
                res+=" "+str(i)
        if res!="":
            return "Missing primes: "+str(res)+"."  
        else:
            return "No prime less than "+str(self.B)+" is missing."

def BatchGenPrimeNumbers(Total):

    assert Total>0
    Anz=Total

    print("Ich erzeuge {} 1024-Bit-Primzahlen.".format(Anz))
    print("Plattform: {} (native byteorder: {})".format(platform.system(), sys.byteorder))

    # https://progressbar-2.readthedocs.io/en/latest/examples.html
    widgets = [progressbar.Percentage(), ' ', progressbar.ETA(), ' ', progressbar.Bar()]
    bar = progressbar.ProgressBar(widgets=widgets, max_value=Anz).start()
    if platform.system()=='Linux' or platform.system().startswith('CYGWIN'):
        # being paranoid, i want to know where my randomness comes from
        with open("/dev/urandom", "rb") as f:
            for i in range(0,Anz):
                x=f.read(128)
                first=x[0] | 0x80
                x=first.to_bytes(1, 'big') + x[1:]
                p=int.from_bytes(x, byteorder='big', signed=False)
                p=int(next_prime(p))
                assert bytes_needed(p)==128
                yield p
                bar.update(i + 1)
    else:
        for i in range(0,Anz):
            yield secrets.randbits(1024)
            bar.update(i + 1)

    bar.finish()

class PrimeDB:
    def __init__(self, DBFile=""):
        if DBFile=='':
            raise ValueError("you have to supply a DBFile-name")
        if not os.path.exists(DBFile):
            raise FileNotFoundError(DBFile+' not found')

        self.DBFile=DBFile
        self.f=open(self.DBFile,"rb")
        self.m=mmap.mmap(self.f.fileno(), 0, access=mmap.ACCESS_READ)
        self.DBSize=self.m.size()

    def __str__(self):
        return "Ich habe {} (size={}), damit Elemente={}.".format(self.DBFile,
                    self.DBSize, self.DBSize>>7)

    def get(self,i):
        offset=i*128
        if offset+128>self.DBSize or offset<0:
            raise IndexError("xxx")
        x=int.from_bytes(self.m[offset:offset+128], byteorder='big', signed=False)
        return x

    def getNumberOfElements(self):
        return self.DBSize>>7


if __name__ == '__main__':
    db=PrimeDB(DBFile="primes.db")
    print(db)
    print(hex(db.get(0)))



